from marshmallow import Schema, fields, ValidationError
from ....application.command.impl.delete_user_impl import DeleteUserCommand


class LoginOutput(Schema):
    token = fields.String()
    message = fields.String()


class LoginResponse:
    def __init__(self, response: DeleteUserCommand):
        self.token: str = response.response

    def response(self):
        try:
            response: LoginOutput = {}
            response["token"] = self.token
            response["message"] = "Login successfully"
            return LoginOutput().load(response)
        except ValidationError as err:
            raise Exception(err.messages)
