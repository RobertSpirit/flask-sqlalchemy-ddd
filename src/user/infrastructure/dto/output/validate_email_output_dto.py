from marshmallow import Schema, fields, ValidationError
from ....application.command.impl.validate_email_impl import ValidateEmailCommand


class ValidateEmailOutput(Schema):
    result = fields.Boolean()
    message = fields.String()


class ValidateEmailResponse:
    def __init__(self, response: ValidateEmailCommand):
        self.request: bool = response.response

    def response(self):
        try:
            response = {}
            response["result"] = self.request
            response["message"] = "User Email Validate Successfully"
            return ValidateEmailOutput().load(response)
        except ValidationError as err:
            raise Exception(err.messages)
