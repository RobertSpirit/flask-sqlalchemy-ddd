from marshmallow import Schema, fields, ValidationError

from ....application.command.impl.create_user_impl import CreateUserCommand


class CreateUserOutput(Schema):
    id = fields.UUID()
    message = fields.String()


class CreateUserResponse:
    def __init__(self, response: CreateUserCommand):
        self.id: str = response.response.id

    def response(self):
        try:
            response: CreateUserOutput = {}
            response["id"] = self.id
            response["message"] = "User Created Successfull"
            return CreateUserOutput().load(response)
        except ValidationError as err:
            raise Exception(err.messages)
