from marshmallow import Schema, fields, ValidationError
from ....application.command.impl.update_user_impl import UpdateUserCommand


class UpdateUserOutput(Schema):
    id = fields.UUID()
    message = fields.String()


class UpdateUserResponse:
    def __init__(self, response: UpdateUserCommand):
        self.id: str = response.response.id

    def response(self):
        try:
            response: UpdateUserOutput = {}
            response["id"] = self.id
            response["message"] = "User Updated Successfully"
            return UpdateUserOutput().load(response)
        except ValidationError as err:
            raise Exception(err.messages)
