from marshmallow import Schema, fields, ValidationError
from ....application.command.impl.delete_user_impl import DeleteUserCommand


class DeleteUserOutput(Schema):
    id = fields.UUID()
    message = fields.String()


class DeleteUserResponse:
    def __init__(self, response: DeleteUserCommand):
        self.id: str = response.response.id

    def response(self):
        try:
            response: DeleteUserOutput = {}
            response["id"] = self.id
            response["message"] = "User Deleted Successfully"
            return DeleteUserOutput().load(response)
        except ValidationError as err:
            raise Exception(err.messages)
