from marshmallow import Schema, fields, ValidationError

from ....application.command.impl.create_user_impl import CreateUserCommand


class GetUserOutput(Schema):
    id = fields.UUID()
    name = fields.String()
    lastName = fields.String()
    surName = fields.String()
    email = fields.Email()
    phone = fields.String()
    gender = fields.String()
    birthday = fields.String()


class GetUserResponse:
    def __init__(self, response: CreateUserCommand):
        self.id = response.response["id"]
        self.name = response.response["name"]
        self.lastName = response.response["lastName"]
        self.surName = response.response["surName"]
        self.email = response.response["email"]
        self.phone = response.response["phone"]
        self.gender = response.response["gender"]
        self.birthday = response.response["birthday"]

    def response(self):
        try:
            response: GetUserOutput = {}
            response["id"] = self.id
            response["name"] = self.name
            response["lastName"] = self.lastName
            response["surName"] = self.surName
            response["email"] = self.email
            response["phone"] = self.phone
            response["gender"] = self.gender
            response["birthday"] = self.birthday
            return GetUserOutput().load(response)
        except ValidationError as err:
            raise Exception(err.messages)
