from dataclasses import field
from marshmallow import Schema, fields, ValidationError
from ....application.command.impl.send_email_verification_command import (
    SendEmailVerificationCommand,
)


class SendEmailVerificationOutput(Schema):
    response = fields.Boolean()
    message = fields.String()


class SendEmailVerificationResponse:
    def __init__(self, response: SendEmailVerificationCommand):
        self.result: bool = response.response

    def response(self) -> SendEmailVerificationOutput:
        try:
            output: SendEmailVerificationOutput = {}
            output["response"] = self.result
            output["message"] = "Send Email Successful"
            return SendEmailVerificationOutput().load(output)
        except ValidationError as err:
            raise Exception(err.messages)
