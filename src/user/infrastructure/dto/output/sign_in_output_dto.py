from marshmallow import Schema, fields, ValidationError

from ....application.command.impl.singIn_impl import singInCommand


class SignInOutput(Schema):
    token = fields.String()
    message = fields.String()


class SignInResponse:
    def __init__(self, response: singInCommand):
        self.token = response.response
    

    def response(self):
        try:
            response: SignInOutput = {}
            response["token"] = self.token
            response["message"] = "Success Login"
            return SignInOutput().load(response)
        except ValidationError as err:
            raise Exception(err.messages)
