from ....application.query.impl.get_user_by_id_query import GetUserByIdCommand
from ....domain.model.user_model import User


class GetUserByIdResponse:
    def __init__(self, response: GetUserByIdCommand):
        self.user: User = response.response

    def response(self):
        try:
            return self.user
        except Exception as err:
            raise Exception(err.messages)
