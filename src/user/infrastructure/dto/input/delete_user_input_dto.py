from marshmallow import Schema, fields, ValidationError, validate


class DeleteUserInput(Schema):
    id = fields.UUID(required=True)


class DeleteUserValidate:
    def __init__(self, request: DeleteUserInput):
        self.request: DeleteUserInput = request

    def validate(self):
        try:
            return DeleteUserInput().load(self.request)
        except ValidationError as err:
            raise Exception(err.messages)
