from marshmallow import Schema, fields, ValidationError, validate


class CreateUserInput(Schema):
    name = fields.String(required=True)
    lastName = fields.String(required=True)
    surName = fields.String(required=True)
    email = fields.Email(required=True)
    password = fields.String(required=True)
    phone = fields.String(required=True)
    gender = fields.String(validate=validate.OneOf(["Hombre", "Mujer", "Otro"]))
    birthday = fields.String(required=True)


class CreateUserValidate:
    def __init__(self, input: CreateUserInput):
        self.input = input

    def validate(self):
        try:
            return CreateUserInput().load(self.input)
        except ValidationError as err:
            raise Exception(err.messages)
