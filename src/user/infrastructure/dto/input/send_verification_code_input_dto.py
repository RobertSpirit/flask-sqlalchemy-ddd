from marshmallow import Schema, fields, ValidationError, validate


class SendVerificationCodeInput(Schema):
    email = fields.Email(required=True)


class SendEmailVerificationCodeValidate:
    def __init__(self, params: SendVerificationCodeInput):
        self.params = params

    def validate(self):
        try:
            return SendVerificationCodeInput().load(self.params)
        except ValidationError as err:
            raise Exception(err.messages)
