from marshmallow import Schema, fields, ValidationError, validate


class UpdateUserInput(Schema):
    name = fields.String(validate=[validate.Length(min=5, max=100)])
    surName = fields.String(validate=validate.Length(min=1, max=100))
    lastName = fields.String(validate=validate.Length(min=1, max=100))
    email = fields.Email()
    password = fields.String(validate=validate.Length(min=1, max=100))
    phone = fields.String(validate=validate.Length(equal=10))
    avatar = fields.String()
    gender = fields.String(validate=validate.OneOf(["Hombre", "Mujer", "Otro"]))
    id = fields.UUID(required=True)


class UpdateUserValidate:
    def __init__(self, params: UpdateUserInput):
        self.params = params

    def validate(self):
        try:
            return UpdateUserInput().load(self.params)
        except ValidationError as err:
            raise Exception(err.messages)
