from marshmallow import Schema, fields, ValidationError


class SignInInput(Schema):
    email = fields.Email(required=True)
    password = fields.String(required=True)


class SigninValidate:
    def __init__(self, input: SignInInput):
        self.input = input

    def validate(self):
        try:
            return SignInInput().load(self.input)
        except ValidationError as err:
            raise Exception(err.messages)
