from marshmallow import Schema, fields, ValidationError


class ValidateEmailInput(Schema):
    code = fields.String(required=True)
    email = fields.Email(required=True)


class ValidateEmail:
    def __init__(self, request):
        self.request = request

    def validate(self):
        try:
            return ValidateEmailInput().load(self.request)
        except ValidationError as err:
            raise Exception(err.messages)
