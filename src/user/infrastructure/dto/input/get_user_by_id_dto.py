from marshmallow import Schema, fields, ValidationError


class GetUserInput(Schema):
    id = fields.UUID(required=True)


class GetUserValidate:
    def __init__(self, input: GetUserInput):
        self.input = input

    def validate(self):
        try:
            return GetUserInput().load(self.input)
        except ValidationError as err:
            raise Exception(err.messages)
