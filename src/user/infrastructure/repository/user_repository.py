from sqlalchemy.orm.attributes import flag_modified
from ...infrastructure.dto.input.create_user_input_dto import CreateUserInput
from ...infrastructure.dto.input.update_user_input_dto import UpdateUserInput
from ...domain.model.user_model import User
from ...connection import db as DB


class Object(object):
    def __init__(self, model, key):
        self.model = model
        self.db = DB
        self.key = key

    def __call__(self, *args, **kwargs):
        try:
            query = self.model.query.filter(
                getattr(self.model, self.key) == args[0]
            ).first()
            response = {}
            for attr, value in query.__dict__.items():
                response[attr] = value
            return response
        except:
            return {}


class Repository(object):
    def __init__(self, model):
        self.model = model
        self.db = DB

    def __getattr__(self, key):
        _key = key.replace("get_by_", "")
        return self.get_by_param(_key)

    def get_by_param(self, key):
        return Object(self.model, key)

    def create(self, params: CreateUserInput):
        newUser = self.model(
            name=params["name"],
            lastName=params["lastName"],
            surName=params["surName"],
            email=params["email"],
            phone=params["phone"],
            gender=params["gender"],
            birthday=params["birthday"],
        )
        self.db.session.add(newUser)
        self.db.session.commit()
        return newUser

    def update(self, params: UpdateUserInput):
        user = self.model.query.get(params["id"])
        for key, value in user.__dict__.items():
            if key != "_sa_instance_state" and key in params:
                setattr(user, key, params[key])
        self.db.session.commit()
        return user

    def deleted(self, id: str):
        user = self.model.query.get(id)
        self.db.session.delete(user)
        self.db.session.commit()
        return user


UserRepository = Repository(User)
