from flask.views import MethodView
from flask_smorest import Blueprint
from flask import jsonify

from ...application.command.impl.singIn_impl import singInCommand

from ..dto.input.sign_in_input_dto import SignInInput, SigninValidate
from ..dto.output.sign_in_output_dto import SignInOutput, SignInResponse


singIn = Blueprint("SignIn", "signIn", url_prefix="/sign-in", description="Sign In")


@singIn.route("")
class SignIn(MethodView):
    @singIn.errorhandler(404)
    def resource_not_found(e):
        return jsonify(error=str(e)), 404

    @singIn.arguments(SignInInput, location="json")
    @singIn.response(200, SignInOutput, content_type="application/json")
    def post(self, body: SignInInput):
        """Sign In"""
        result: SignInInput = SigninValidate(body).validate()
        return SignInResponse(singInCommand(result)).response()

