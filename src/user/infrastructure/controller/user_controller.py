from flask.views import MethodView
from flask_smorest import Blueprint
from flask import jsonify


from ..dto.input import (
    CreateUserInput,
    CreateUserValidate,
    DeleteUserInput,
    UpdateUserInput,
    DeleteUserValidate,
    UpdateUserValidate,
)
from ..dto.input.get_user_by_id_dto import GetUserInput, GetUserValidate

from ..dto.output.create_user_output_dto import CreateUserOutput, CreateUserResponse
from ..dto.output.delete_user_output_dto import DeleteUserOutput, DeleteUserResponse
from ..dto.output.update_user_output_dto import UpdateUserOutput, UpdateUserResponse
from ..dto.output.get_user_output_dto import GetUserOutput, GetUserResponse

from ...application.command.impl.create_user_impl import CreateUserCommand
from ...application.command.impl.delete_user_impl import DeleteUserCommand
from ...application.command.impl.update_user_impl import UpdateUserCommand
from ...application.query.impl.get_user_by_id_query import GetUserByIdCommand


user = Blueprint("User", "user", url_prefix="/user", description="User")


@user.route("")
class User(MethodView):
    @user.errorhandler(404)
    def resource_not_found(e):
        return jsonify(error=str(e)), 404

    @user.arguments(GetUserInput, location="query")
    @user.response(200, GetUserOutput, content_type="application/json")
    def get(self, body: GetUserInput):
        """Create a new user"""
        result: GetUserInput = GetUserValidate(body).validate()
        return GetUserResponse(GetUserByIdCommand(result)).response()

    @user.arguments(CreateUserInput, location="json")
    @user.response(200, CreateUserOutput, content_type="application/json")
    @user.response(422)
    def post(self, body: CreateUserInput):
        """Create a new user"""
        result: CreateUserInput = CreateUserValidate(body).validate()
        return CreateUserResponse(CreateUserCommand(result)).response()

    @user.arguments(UpdateUserInput, location="json")
    @user.response(200, UpdateUserOutput, content_type="application/json")
    @user.response(422)
    def put(self, body: UpdateUserInput):
        """Update user"""
        result: UpdateUserInput = UpdateUserValidate(body).validate()
        return UpdateUserResponse(UpdateUserCommand(result)).response()

    @user.arguments(DeleteUserInput, location="json")
    @user.response(200, DeleteUserOutput, content_type="application/json")
    @user.response(422)
    def delete(self, body: DeleteUserInput):
        """delete user"""
        result: DeleteUserInput = DeleteUserValidate(body).validate()
        return DeleteUserResponse(DeleteUserCommand(result)).response()
