from flask.views import MethodView
from flask_smorest import Blueprint
from flask import jsonify


from ..dto.output.send_email_verification_output_dto import (
    SendEmailVerificationOutput,
    SendEmailVerificationResponse,
)
from ..dto.output.validate_email_output_dto import (
    ValidateEmailOutput,
    ValidateEmailResponse,
)
from ..dto.input.validate_email_input_dto import ValidateEmail, ValidateEmailInput
from ..dto.input.send_verification_code_input_dto import (
    SendEmailVerificationCodeValidate,
    SendVerificationCodeInput,
)
from ..dto.input import CreateUserInput

from ...application.command.impl.create_user_impl import CreateUserCommand
from ...application.command.impl.send_email_verification_command import (
    SendEmailVerificationCommand,
)
from ...application.command.impl.validate_email_impl import ValidateEmailCommand


mail = Blueprint("Email", "email", url_prefix="/", description="Email")


class Email(MethodView):
    @mail.errorhandler(404)
    def resource_not_found(e):
        return jsonify(error=str(e)), 404

    @mail.route("/sendCodeByEmail", methods=["POST"])
    @mail.arguments(SendVerificationCodeInput, location="json")
    @mail.response(200, SendEmailVerificationOutput, content_type="application/json")
    def post(body: SendVerificationCodeInput):
        """Send code activation account"""
        result: SendVerificationCodeInput = SendEmailVerificationCodeValidate(
            body
        ).validate()
        return SendEmailVerificationResponse(
            SendEmailVerificationCommand(result["email"])
        ).response()

    @mail.route("/validateCodeByEmail", methods=["POST"])
    @mail.arguments(ValidateEmailInput, location="json")
    @mail.response(200, ValidateEmailOutput, content_type="application/json")
    def post(body: CreateUserInput):
        """Validate activation code"""
        result: ValidateEmailInput = ValidateEmail(body).validate()
        return ValidateEmailResponse(ValidateEmailCommand(result)).response()
