# Import controllers
from .infrastructure.controller import user, mail, singIn

# Import containers
from .provider import container

# Import modules
import os
from flask_sqlalchemy import SQLAlchemy
from dotenv import load_dotenv
from flask import Flask
from flask_smorest import Api

# load configuration .env
load_dotenv()

# configuration
class Config:
    SQLALCHEMY_DATABASE_URI = "{}://{}:{}@{}:{}/{}".format(
        os.getenv("DATABASE_NAME"),
        os.getenv("DATABASE_USER"),
        os.getenv("DATABASE_PASSWORD"),
        os.getenv("DATABASE_HOST"),
        os.getenv("DATABASE_PORT"),
        os.getenv("DATABASE_DB"),
    )
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    API_VERSION = 0.1
    API_TITLE = "API test"
    OPENAPI_VERSION = "3.0.2"
    OPENAPI_URL_PREFIX = "/"
    OPENAPI_REDOC_PATH = "/redoc"
    OPENAPI_REDOC_URL = (
        "https://cdn.jsdelivr.net/npm/redoc@next/bundles/redoc.standalone.js"
    )
    OPENAPI_SWAGGER_UI_PATH = "/swagger-ui"
    OPENAPI_SWAGGER_UI_URL = "https://cdnjs.cloudflare.com/ajax/libs/swagger-ui/3.24.2/"
    API_SPEC_OPTIONS = {"info": {"description": "Technical test documentation"}}


# Flask start
app = Flask(__name__)

# Add configuration
app.config.from_object(Config)
api = Api(app)
SQLAlchemy(app)

# Register containter
app.container = container

# register modules
api.register_blueprint(user)
api.register_blueprint(mail)
api.register_blueprint(singIn)

