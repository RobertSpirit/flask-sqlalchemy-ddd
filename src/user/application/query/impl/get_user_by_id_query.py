from ....domain.model.user_model import User
from ... import ICommandBase
from ..handler.get_user_by_id_query import GetUserByIdHandler


class GetUserByIdCommand(ICommandBase):
    """
        Get user by id

        :param id: Id to User
        
        :return: returns User
    """

    def __init__(self, request: object):
        self.id: str = request["id"]
        self.response: User = self.execute()

    def execute(self) -> User:
        return GetUserByIdHandler(self.id).execute()
