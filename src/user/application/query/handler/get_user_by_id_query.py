from ....domain.contracts.services.user_services_interface import IUserServices
from ....domain.model.user_model import User
from ... import ICommandHandler
from dependency_injector.wiring import Provide, inject


@inject
class GetUserByIdHandler(ICommandHandler):
    def __init__(self, id: str, userService=Provide["userService"]):
        self.id: str = id
        self.userService: IUserServices = userService

    def execute(self) -> User:
        return self.userService.get_user_by_id(self.id)
