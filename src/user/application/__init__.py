from abc import ABC, abstractmethod
from typing import Generic, TypeVar


class ICommandBase(ABC):
    """
    Command Inferfaces
    """


class ICommandHandler:
    T = TypeVar("T")

    @abstractmethod
    def execute(command: ICommandBase) -> Generic[T]:
        raise NotImplementedError("Please implement in subclass")
