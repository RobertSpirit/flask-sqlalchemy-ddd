from dependency_injector.wiring import Provide, inject


from ....domain.contracts.services.user_email_service_interface import (
    IUserEmailServices,
)
from ... import ICommandHandler


@inject
class ValidateEmailHandler(ICommandHandler):
    def __init__(
        self,
        email: str,
        code: str,
        userEmailService: IUserEmailServices = Provide["userEmailService"],
    ):
        self.email: str = email
        self.code: str = code
        self.userEmailService: IUserEmailServices = userEmailService

    def execute(self):
        return self.userEmailService.validate_user_email(self.email, self.code)
