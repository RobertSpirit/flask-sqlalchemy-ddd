from dependency_injector.wiring import Provide, inject

from ....domain.contracts.services.user_services_interface import IUserServices
from ... import ICommandHandler


@inject
class singInHandler(ICommandHandler):
    def __init__(
        self,
        email: str, 
        password: str,
        userService: IUserServices = Provide["userService"],
    ):
        self.email: str =  email
        self.password: str = password
        self.userService: IUserServices = userService

    def execute(self) -> str:
        return self.userService.singIn(self.email, self.password)
