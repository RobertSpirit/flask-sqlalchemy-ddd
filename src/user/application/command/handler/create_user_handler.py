from dependency_injector.wiring import Provide, inject

from ....infrastructure.dto.input.create_user_input_dto import CreateUserInput
from ....domain.contracts.services.user_services_interface import IUserServices
from ... import ICommandHandler


@inject
class CreateUserHandler(ICommandHandler):
    def __init__(
        self,
        input: CreateUserInput,
        userService: IUserServices = Provide["userService"],
    ):
        self.input: CreateUserInput = input.input
        self.userService: IUserServices = userService

    def execute(self):
        return self.userService.create_user(self.input)
