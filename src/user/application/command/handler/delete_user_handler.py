from dependency_injector.wiring import Provide, inject

from ....domain.contracts.services.user_services_interface import IUserServices
from ... import ICommandHandler


@inject
class DeleteUserHandler(ICommandHandler):
    def __init__(
        self,
        id: str,
        userService: IUserServices = Provide["userService"],
    ):
        self.id: str = id
        self.userService: IUserServices = userService

    def execute(self):
        return self.userService.delete_user(self.id)
