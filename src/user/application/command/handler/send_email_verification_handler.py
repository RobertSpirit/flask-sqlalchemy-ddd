from dependency_injector.wiring import Provide, inject


from ....domain.contracts.services.user_email_service_interface import (
    IUserEmailServices,
)
from ....domain.contracts.services.user_services_interface import IUserServices
from ... import ICommandHandler


@inject
class SendEmailVerificationHandler(ICommandHandler):
    def __init__(
        self,
        email: str,
        userEmailService: IUserEmailServices = Provide["userEmailService"],
    ):
        self.email: str = email
        self.userEmail: IUserEmailServices = userEmailService

    def execute(self) -> bool:
        return self.userEmail.sendEmailVerificationCode(self.email)
