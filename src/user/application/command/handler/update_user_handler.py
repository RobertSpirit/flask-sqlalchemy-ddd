from dependency_injector.wiring import Provide, inject


from ....domain.contracts.services.user_services_interface import IUserServices
from ... import ICommandHandler


@inject
class UpdateUserHandler(ICommandHandler):
    def __init__(
        self,
        params,
        userService: IUserServices = Provide["userService"],
    ):
        self.params = params.params
        self.userService: IUserServices = userService

    def execute(self):
        return self.userService.update_user(self.params)
