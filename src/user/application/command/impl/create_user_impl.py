from ....domain.model.user_model import User
from ....application import ICommandBase
from ....application.command.handler.create_user_handler import CreateUserHandler
from ....infrastructure.dto.input.create_user_input_dto import CreateUserInput


class CreateUserCommand(ICommandBase):  
    """
        Create user account

        :param name: name to new user
        :param lastName: lastname to new user
        :param surName: surname to new user
        :param email: email to new user
        :param password: password to new user
        :param phone: phone to new user
        :param gender: gender to new user
        :param birthday: birthday to new user
        
        :return: returns User
    """

    def __init__(self, request: CreateUserInput):
        self.input: CreateUserInput = request
        self.response = self.execute()

    def execute(self) -> User:
        return CreateUserHandler(self).execute()
