from ....infrastructure.dto.input.delete_user_input_dto import DeleteUserInput
from ....application import ICommandBase
from ....application.command.handler.delete_user_handler import DeleteUserHandler
from ....domain.model.user_model import User


class DeleteUserCommand(ICommandBase):
    """
        Deleted user account

        :param id: Id to User
        :return: returns User
    """
    def __init__(self, request: DeleteUserInput):
        self.id: str = request["id"]
        self.response: User = self.execute()

    def execute(self) -> User:
        return DeleteUserHandler(self.id).execute()
