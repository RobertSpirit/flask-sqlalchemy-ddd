from ....application import ICommandBase
from ....application.command.handler.validate_email_handler import ValidateEmailHandler
from ....domain.model.user_model import User
from ....infrastructure.dto.input.validate_email_input_dto import ValidateEmailInput


class ValidateEmailCommand(ICommandBase):
    """
        Validate account by email

        :param email: email to user
        :param code: code to validate
        
        :return: returns User
    """

    def __init__(self, request: ValidateEmailInput):
        self.email: str = request["email"]
        self.code: str = request["code"]
        self.response: User = self.execute()

    def execute(self) -> User:
        return ValidateEmailHandler(self.email, self.code).execute()
