from ... import ICommandBase
from ..handler.singIn_handler import singInHandler
from ....infrastructure.dto.input.sign_in_input_dto import SignInInput
from ....infrastructure.dto.output.login_output_dto import LoginOutput


class singInCommand(ICommandBase):
    """
        Sign In

        :param email: email to User
        :param password: password to User

        :return: returns token
    """

    def __init__(self, request: SignInInput):
        print(request)
        self.email: str = request["email"]
        self.password: str = request["password"]
        self.response: LoginOutput = self.execute()

    def execute(self) -> LoginOutput:
        return singInHandler(self.email, self.password).execute()
