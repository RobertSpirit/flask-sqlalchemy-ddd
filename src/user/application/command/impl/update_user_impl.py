from ....application import ICommandBase
from ....domain.model.user_model import User
from ....application.command.handler.update_user_handler import UpdateUserHandler
from ....infrastructure.dto.input.update_user_input_dto import UpdateUserInput


class UpdateUserCommand(ICommandBase):
    """
        Update user account

        :param id: id to user
        :param name: name to user
        :param lastName: lastname to user
        :param surName: surname to user
        :param email: email to user
        :param password: password to user
        :param phone: phone to user
        :param gender: gender to user
        :param birthday: birthday to user
        
        :return: returns User
    """

    def __init__(self, params: UpdateUserInput):
        self.params: UpdateUserInput = params
        self.response: User = self.execute()

    def execute(self) -> User:
        return UpdateUserHandler(self).execute()
