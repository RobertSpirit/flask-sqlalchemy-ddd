from ....application import ICommandBase
from ....application.command.handler.send_email_verification_handler import (
    SendEmailVerificationHandler,
)


class SendEmailVerificationCommand(ICommandBase):
    """
        Send Verification Code Email

        :param email: email to send email
        
        :return: returns boolean
    """

    def __init__(self, email: str):
        self.email: str = email
        self.response: bool = self.execute()

    def execute(self) -> bool:
        return SendEmailVerificationHandler(self.email).execute()
