from .domain.services.user_email_service import UserEmailService
from .domain.services.user_service import UserService
from .domain.factory.factory import Container

# import handlers
from .application.command.handler.create_user_handler import CreateUserHandler
from .application.command.handler.delete_user_handler import DeleteUserHandler
from .application.command.handler.singIn_handler import singInHandler
from .application.command.handler.send_email_verification_handler import (
    SendEmailVerificationHandler,
)
from .application.command.handler.update_user_handler import UpdateUserHandler
from .application.command.handler.validate_email_handler import ValidateEmailHandler
from .application.query.handler.get_user_by_id_query import GetUserByIdHandler


# import services
Services = [UserService, UserEmailService]
Handlers = [
    CreateUserHandler,
    DeleteUserHandler,
    singInHandler,
    SendEmailVerificationHandler,
    UpdateUserHandler,
    ValidateEmailHandler,
    GetUserByIdHandler,
]
Models = []

container = Container()
container.wire(modules=Services + Handlers + Models)
