from ..exceptions import Error


class UserEmailError:
    def user_code_not_valid(self):
        return Error("User code not valid")
