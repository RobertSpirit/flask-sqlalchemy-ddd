from ..exceptions import Error


class UserError:
    def user_not_save(self):
        Error("User not save")

    def user_not_found(self):
        Error("User not found")

    def user_exist(self):
        Error("Email has register")

    def user_not_updated(self):
        Error("User not updated")
