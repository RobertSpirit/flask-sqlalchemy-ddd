import random


def generate_code() -> str:
    code: str = str(random.randrange(111111, 999999))
    return code
