from django.contrib.auth.hashers import make_password, check_password


class EncryptUserPassword:
    def encrypt(password: str) -> str:
        return make_password(password)

    def decrypt(request: str, password: str) -> bool:
        return check_password(request, password)
