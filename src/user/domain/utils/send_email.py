import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText


class Email:
    def sendEmail(subject, html, email) -> None:
        msg = MIMEMultipart("alternative")
        msg["Subject"] = subject
        msg["From"] = "System"
        msg["To"] = email

        part = MIMEText(html, "html")
        msg.attach(part)

        from_addr: str = "axolotlcode.tech@gmail.com"
        username: str = "axolotlcode.tech@gmail.com"
        password: str = "Contrasena123"
        server = smtplib.SMTP("smtp.gmail.com:587")
        server.starttls()
        server.login(username, password)
        server.sendmail(from_addr, email, msg.as_string())
        server.quit()
