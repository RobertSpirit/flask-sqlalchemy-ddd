from xmlrpc.client import Boolean
from dependency_injector.wiring import Provide, inject

from ...domain.contracts.repository.user_repository_interfaces import IRepositoryUser
from ...domain.contracts.services.user_email_service_interface import (
    IUserEmailServices,
)
from ...domain.exceptions.user_email_exception import UserEmailError
from ...domain.exceptions.user_exception import UserError
from ...domain.model.user_model import User
from ...domain.utils.generate_code import generate_code
from ...domain.utils.send_email import Email


@inject
class UserEmailService(IUserEmailServices):
    def __init__(self, repository: IRepositoryUser = Provide["userRepository"]):
        self.user_email_errors: UserEmailError = UserEmailError()
        self.user_errors: UserError = UserError()
        self.userRepository: IRepositoryUser = repository

    def validate_user_email(self, email: str, code: str) -> bool:
        user = self.userRepository.get_by_email(email)
        if not user["verificationCode"] == code:
            self.user_email_errors.user_code_not_valid()
        user["status"] = True
        user["verificationCode"] = None
        update = self.userRepository.update(user)
        if not update:
            self.user_errors.user_not_updated()
        return True

    def sendEmailVerificationCode(self, email: str) -> bool:
        user: User = self.userRepository.get_by_email(email)
        if not user:
            self.user_errors.user_not_found()
        code: str = generate_code()
        user["verificationCode"] = code
        update: bool = self.userRepository.update(user)
        if not update:
            self.user_email_errors.user_code_not_valid()
        html: str = (
            open("src/user/presentation/views/send_veritication_code.html")
            .read()
            .format(email=email, code=code)
        )
        Email.sendEmail("Welcome to system", html, email)
        return True
