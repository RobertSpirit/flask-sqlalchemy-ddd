from dependency_injector.wiring import Provide, inject

from ...domain.contracts.repository.user_repository_interfaces import IRepositoryUser
from ...domain.contracts.services.user_services_interface import IUserServices
from ...domain.exceptions.user_exception import UserError
from ...domain.model.user_model import User
from ...infrastructure.dto.input.update_user_input_dto import UpdateUserInput
from ...infrastructure.dto.input.create_user_input_dto import CreateUserInput


@inject
class UserService(IUserServices):
    def __init__(
        self,
        repository: IRepositoryUser = Provide["userRepository"],
        firebase=Provide["firebaseClient"],
    ):
        self.user_errors: UserError = UserError()
        self.user_repository: IRepositoryUser = repository
        self.firebaseClient = firebase

    def create_user(self, input: CreateUserInput) -> User:
        existUser = self.user_repository.get_by_email(input["email"])
        if existUser:
            self.user_errors.user_exist()
        newUser = self.user_repository.create(input)
        if not newUser:
            self.user_errors.user_not_save()
        self.firebaseClient.create_user_with_email_and_password(
            input["email"], input["password"]
        )
        return newUser

    def update_user(self, params: UpdateUserInput) -> User:
        updateUser: User = self.user_repository.update(params)
        if not updateUser:
            raise self.user_errors.user_not_updated()
        return updateUser

    def delete_user(self, id: str) -> User:
        existUserById: User = self.user_repository.get_by_id(id)
        if not existUserById:
            self.user_errors.user_not_found()
        return self.user_repository.deleted(id)

    def get_user_by_id(self, id: str) -> User:
        user = self.user_repository.get_by_id(id)
        if not user:
            raise self.user_errors.user_not_found()
        return user

    def singIn(self, email:str, password:str) -> str:
        print(email, password)
        existUser = self.user_repository.get_by_email(email)
        if not existUser:
            self.user_errors.user_not_found()
        singIn = self.firebaseClient.sign_in_with_email_and_password(email, password)
        return singIn['idToken']
