from dependency_injector import containers, providers
from flask_sqlalchemy import SQLAlchemy
import pyrebase

from ...domain.services.user_email_service import UserEmailService
from ...domain.services.user_service import UserService
from ...infrastructure.repository.user_repository import UserRepository

firebaseConfig = {
    "apiKey": "AIzaSyBsixgP5kUzuPn2JXVrwYL6ZeuuJvL8pps",
    "authDomain": "testingleach.firebaseapp.com",
    "databaseURL": "https://testingleach-default-rtdb.firebaseio.com",
    "projectId": "testingleach",
    "storageBucket": "testingleach.appspot.com",
    "messagingSenderId": "648209300412",
    "appId": "1:648209300412:web:b82c7afb00ca07531d89b9",
    "measurementId": "G-LS962261NK",
}


class Container(containers.DeclarativeContainer):
    # conection with mysql
    db = providers.Singleton(SQLAlchemy)

    # import services here
    userService = providers.Singleton(UserService)
    userEmailService = providers.Singleton(UserEmailService)

    # import repositories
    userRepository = providers.Object(UserRepository)

    firebaseClient = providers.Object(pyrebase.initialize_app(firebaseConfig).auth())
