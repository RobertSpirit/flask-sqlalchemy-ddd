from abc import ABC, abstractmethod

from ....infrastructure.dto.input.update_user_input_dto import UpdateUserInput
from ....infrastructure.dto.input.create_user_input_dto import CreateUserInput


class IRepositoryUser(ABC):
    @abstractmethod
    def create(self, params: CreateUserInput):
        raise NotImplementedError("Please implement in subclass")

    @abstractmethod
    def get_by_email(self, email: str):
        raise NotImplementedError("Please implement in subclass")

    @abstractmethod
    def update(self, id: str, params: UpdateUserInput):
        raise NotImplementedError("Please implement in subclass")

    @abstractmethod
    def deleted(self, id: str):
        raise NotImplementedError("Please implement in subclass")
