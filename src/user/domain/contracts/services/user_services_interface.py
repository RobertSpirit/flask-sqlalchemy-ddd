from abc import ABC, abstractmethod

from ....infrastructure.dto.input.update_user_input_dto import UpdateUserInput
from ....infrastructure.dto.input.create_user_input_dto import CreateUserInput


class IUserServices(ABC):
    @abstractmethod
    def create_user(self, input: CreateUserInput):
        raise NotImplementedError("Please implement in subclass")

    @abstractmethod
    def update_user(self, params: UpdateUserInput):
        raise NotImplementedError("Please implement in subclass")

    @abstractmethod
    def delete_user(self, id: str):
        raise NotImplementedError("Please implement in subclass")

    @abstractmethod
    def get_user_by_id(self, id: str):
        raise NotImplementedError("Please implement in subclass")
    
    @abstractmethod
    def singIn(self, email: str, password: str):
        raise NotImplementedError("Please implement in subclass")
