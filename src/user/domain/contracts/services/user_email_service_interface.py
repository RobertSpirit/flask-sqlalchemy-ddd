from abc import ABC, abstractmethod


class IUserEmailServices(ABC):
    @abstractmethod
    def validate_user_email(self, email: str, code: str):
        raise NotImplementedError("Please implement in subclass")

    @abstractmethod
    def sendEmailVerificationCode(self, email: str):
        raise NotImplementedError("Please implement in subclass")
