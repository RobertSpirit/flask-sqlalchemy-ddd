from ...connection import db
from uuid import uuid4


class User(db.Model):
    id = db.Column("id", db.String(80), primary_key=True, default=uuid4)
    name = db.Column(db.String(80))
    lastName = db.Column(db.String(80))
    surName = db.Column(db.String(80))
    email = db.Column(db.String(80))
    phone = db.Column(db.String(80))
    gender = db.Column(db.String(80))
    birthday = db.Column(db.String(80))
    status = db.Column(db.Boolean(), default=False)
    verificationCode = db.Column(db.String(80), nullable=True)
    resetPasswordToken = db.Column(db.String(80), nullable=True)
    resetPasswordExpires = db.Column(db.String(80), nullable=True)

    def __init__(self, name, lastName, surName, email, phone, gender, birthday):
        self.name = name
        self.lastName = lastName
        self.surName = surName
        self.email = email
        self.phone = phone
        self.gender = gender
        self.birthday = birthday
