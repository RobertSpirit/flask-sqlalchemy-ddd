from src.user.index import app
from src.user.connection import db

        
with app.app_context():
    db.init_app(app)
    db.create_all(app=app)
    
if(__name__) == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)