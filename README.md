# Technical test

This is a technical test

## Installation

The project is dockerized in order to have a faster installation.
In order to install, the following command is executed

```bash
docker-compose build
docker-compose up -d
```

## Documentation
To view the apis documentation, please click on the following links

Swagger 
```bash
http://localhost/swagger-ui
```

Redoc 
```bash
http://localhost/redoc
```



## Functionalities
 - Create user
 - Update user
 - Deleted user
 - Send Verification Code
 - Validate Code
 - SignIn

these services have interaction with both the MYSQL database and the firebase service for registration and logging.



## Architecture

In this project the DDD (Domain Driven Design) architecture is used using each of the layers to better manage responsibilities.

![alt text](https://www.hibit.dev/images/posts/2021/ddd_layers.png)

Some design patterns such as chain of responsibility, dependency injection and some others were also used.


## License
The development of the template was carried out by Roberto Espiritu
[@RobertSpirit](https://gitlab.com/RobertSpirit)

